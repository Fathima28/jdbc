package com.hcl.controller;

import java.util.List;

import com.hcl.dao.IAdminDao;
import com.hcl.dao.IAdminImpl;
import com.hcl.model.Admin;
import com.hcl.model.Loanee;

public class AdminController {
	
	IAdminDao dao=new IAdminImpl();
	
	public int adminAuthen(String uname,String pswd) {
		Admin admin=new Admin(uname,pswd);
		return dao.adminAuthen(admin);		
	}
	public List<Loanee> ViewAllLoanee(){
		return dao.viewAllLoanee();
	}
	
	public int addLoanee(int lid, String lname, long amt, int rateofin, int years,float interest)
	{
		Loanee loan=new Loanee(lid,lname,amt,rateofin,years,interest);
		return dao.addLoanee(loan);
	}
	
	public int updateAmount(int lid,long amt,float interest) {
		Loanee loan=new Loanee();
		loan.setLid(lid);
		loan.setAmt(amt);
		loan.setInterest(interest);
		return dao.updateAmount(loan);		
	}

	public int removeLoanee(int lid) {
		Loanee loan=new Loanee();
		loan.setLid(lid);
		return dao.removeLoanee(loan);
		
	}
}
