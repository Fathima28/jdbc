package com.hcl.model;

public class Admin {
private String uname;
private String pswd;
public Admin()
{
}
public Admin(String uname, String pswd) {
	super();
	this.uname = uname;
	this.pswd = pswd;
}
public String getUname() {
	return uname;
}
public void setUname(String uname) {
	this.uname = uname;
}
public String getPswd() {
	return pswd;
}
public void setPswd(String pswd) {
	this.pswd = pswd;
}



}
