package com.hcl.model;
public class Loanee {
private Integer lid;
private String lname;
private Long amt;
private Integer rateofin;
private Integer years;
private Float interest;
public Loanee()
{	
}
public Loanee(Integer lid, String lname, Long amt, Integer rateofin, Integer years,Float interest) {
	super();
	this.lid = lid;
	this.lname = lname;
	this.amt = amt;
	this.rateofin = rateofin;
	this.years = years;
	this.interest = interest;
}
public Integer getLid() {
	return lid;
}
public void setLid(Integer lid) {
	this.lid = lid;
}
public String getLname() {
	return lname;
}
public void setLname(String lname) {
	this.lname = lname;
}
public Long getAmt() {
	return amt;
}
public void setAmt(Long amt) {
	this.amt = amt;
}
public Integer getRateofin() {
	return rateofin;
}
public void setRateofin(Integer rateofin) {
	this.rateofin = rateofin;
}
public Integer getYears() {
	return years;
}
public void setYears(Integer years) {
	this.years = years;
}
public Float getInterest() {
	return interest;
}
public void setInterest(Float interest) {
	this.interest = interest;
}

}

