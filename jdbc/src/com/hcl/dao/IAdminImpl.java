package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.Loanee;
import com.hcl.util.Db;
import com.hcl.util.Query;

public class IAdminImpl implements IAdminDao{
	PreparedStatement pst=null;
	ResultSet rs=null;
	int result;	

	@Override
	public int adminAuthen(Admin admin) {
		result=0;
		try {
			pst = Db.getConnection().prepareStatement(Query.adminAuth);
			pst.setString(1, admin.getUname());
			pst.setString(2, admin.getPswd());
			rs=pst.executeQuery();
			while (rs.next())
			{
				result++;
			}		
		} catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception in Admin Authentication");
		} finally {
			try
			{
				
				rs.close();
				Db.getConnection().close();
				pst.close();
			}
			catch(ClassNotFoundException | SQLException e) {	
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<Loanee> viewAllLoanee() {
		List <Loanee> list=new ArrayList<Loanee>();
			try {
				pst = Db.getConnection().prepareStatement(Query.viewAllLoanee);
				rs=pst.executeQuery();
				while (rs.next())
				{
					Loanee loan=new Loanee(rs.getInt("lid"),rs.getString(2),rs.getLong("amt"),rs.getInt("rateofin"),rs.getInt("year"),rs.getFloat("interest"));
				list.add(loan);
				}		
			} catch(ClassNotFoundException | SQLException e) {
				System.out.println("Exception in View ALL Loanee");
				e.printStackTrace();
			} finally {
				try
				{
					Db.getConnection().close();
					pst.close();
					rs.close();
				}
				catch(ClassNotFoundException | SQLException e) {	
				}
			}
		
		return list;
	}

	@Override
	public int addLoanee(Loanee loan) {
		result=0;
	try {
			pst = Db.getConnection().prepareStatement(Query.addLoanee);
			pst.setInt(1, loan.getLid());
			pst.setString(2, loan.getLname());
			pst.setLong(3, loan.getAmt());
			pst.setFloat(4,loan.getRateofin());
			pst.setInt(5, loan.getYears());
			pst.setFloat(6,loan.getInterest());
			result=pst.executeUpdate();	
		} catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception in AddLoanee");
		} finally {
			try
			{
				pst.close();
				rs.close();
				Db.getConnection().close();
			}
			catch(ClassNotFoundException | SQLException e) {	
			}
		}
		
		return result;
	}


	@Override
	public int updateAmount(Loanee loan) {
		result=0;
		try {
			pst = Db.getConnection().prepareStatement(Query.updateAmount);
			pst.setLong(1, loan.getAmt());
			pst.setFloat(2, loan.getInterest());
			pst.setInt(3,loan.getLid());
			result=pst.executeUpdate();	
		} catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception in UpdateAmount");
			e.printStackTrace();
		} finally {
			try
			{
				pst.close();
				rs.close();
				Db.getConnection().close();
			}
			catch(ClassNotFoundException | SQLException e) {	
			}
		}
		
		return result;	
	}

	@Override
	public int removeLoanee(Loanee loan) {
		result=0;
		try {
			pst = Db.getConnection().prepareStatement(Query.removeLoanee);
			pst.setInt(1, loan.getLid());
			result=pst.executeUpdate();	
		} catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception in RemoveLoanee");
		} finally {
			try
			{
				pst.close();
				rs.close();
				Db.getConnection().close();
			}
			catch(ClassNotFoundException | SQLException e) {	
			}
		}
		
		return result;
		
	}
	

}
