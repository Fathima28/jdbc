package com.hcl.dao;

import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.Loanee;

public interface IAdminDao {
	    public int adminAuthen(Admin admin);
		public List<Loanee> viewAllLoanee();
		public int addLoanee(Loanee loan);
		public int updateAmount(Loanee loan);
		public int removeLoanee(Loanee loan);
	
	

}
