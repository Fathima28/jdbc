package com.hcl.view;

import java.util.List;
import java.util.Scanner;

import com.hcl.controller.AdminController;
import com.hcl.model.Loanee;

public class Main {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter Username,Password");
		String ip[]=s.nextLine().split(",");
		AdminController control=new AdminController();
		int result=0;
		result=control.adminAuthen(ip[0],ip[1]);
		if(result>0)
		{
			System.out.println("Hi "+ ip[0] +",Welcome to admin page");
			int cont=0;
			do
			{
				System.out.println("1.Add Loanee\n2.Remove Loanee\n3.Update Amount\n4.View All Loanee");
				int option=s.nextInt();
				int lid=0,years=0;
				String lname="";
				long amt=0L;
				int rateofin=0;
				float interest=0.0f;
				switch(option)
				{
				case 1:
					System.out.println("Enter LoaneeID");
					lid=s.nextInt();
					s.nextLine();
					System.out.println("Enter Loanee Name");
					lname=s.nextLine();
					System.out.println("Enter Loan Amount");
					amt=s.nextLong();
					System.out.println("Enter Rate of Interest");
					rateofin=s.nextInt();
					System.out.println("Enter No.of.Years");
					years=s.nextInt();
					interest=(amt*rateofin*years)/100;
					result=control.addLoanee(lid,lname,amt,rateofin,years,interest);
					if(result>0) {
						System.out.println(lid+"Added Sucessfully!!!");
					}
					break;
				case 2:
					System.out.println("Enter Loanee ID to remove");
					lid=s.nextInt();
					result=control.removeLoanee(lid);
					System.out.println((result>0)? lid + " Removed Successfully" : lid + " Remove was Failed");
					break;
				case 3:
					System.out.println("Enter Loanee ID to Update Loan Amount:");
					lid=s.nextInt();
					System.out.println("Enter loan amount to update");
					amt=s.nextLong();
					interest=(amt*rateofin*years)/100;
					result=control.updateAmount(lid,amt,interest);
					System.out.println((result>0)? lid + " Updated Successfully" : lid + " Update was Failed");
				case 4:
					List<Loanee> list=control.ViewAllLoanee();
					if(list.size()>0) {
						System.out.println("Loaneeid,Loaneename,Amount,RateOfInterest,No.of.Years,interest");
						for(Loanee l:list)
						{
							System.out.println(l.getLid()+ "," + l.getLname()+ "," +l.getAmt()+","+l.getRateofin()+","+l.getYears()+","+l.getInterest());
						}
					}
					else {
						System.out.println("No Records found");
					}
					
					break;
					default:
						System.out.println("Invalid option");
				}
						System.out.println("Would you like to continue,then press 1");
		         		cont=s.nextInt();					
			}while(cont==1);
		}	
		else {
			System.out.println("Username and Password Incorrect");
		}
		System.out.println("Process Done!!!");
		s.close();
	}
	}
		
	
